package com.ks.ejemplos;

/**
 * Created by Miguel on 19/09/2016.
 */
public class comida
{
    private String tipo;
    private String comida;

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public String getComida()
    {
        return comida;
    }

    public void setComida(String comida)
    {
        this.comida = comida;
    }
}
