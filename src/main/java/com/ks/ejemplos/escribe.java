package com.ks.ejemplos;

/**
 * Created by Miguel on 19/09/2016.
 */
public class escribe
{
    private comida alimento;

    public void ahora()
    {
        if (alimento != null)
        {
            System.out.println(alimento.getTipo() + ":" + alimento.getComida());
        }
        else
        {
            System.out.println("No se ha asignado la injeccion de codigo");
        }
    }

    public void setAlimento(comida alimento)
    {
        this.alimento = alimento;
    }
}
